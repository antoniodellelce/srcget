# vim:syntax=sh

latest="awk/gnuplot.latest.awk"
srcurl="http://sourceforge.net/projects/gnuplot/files/gnuplot/"
extension_input="tar.gz"
#http://tenet.dl.sourceforge.net/project/gnuplot/gnuplot/5.0.3/gnuplot-5.0.3.tar.gz
#https://netcologne.dl.sourceforge.net/project/gnuplot/gnuplot/5.0.5/gnuplot-5.0.5.tar.gz
custom_url_prefix="https://netcologne.dl.sourceforge.net/project/gnuplot/gnuplot/"
custom_url_postfix=".${extension_input}"
custom_file_postfix=".${extension_input}"
