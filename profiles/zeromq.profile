# vim:syntax=sh

latest="awk/zeromq.latest.awk"
baseurl="https://github.com/zeromq/libzmq"
srcurl="${baseurl}/releases"
extension_input="tar.gz"
sep='"'
custom_url_prefix="${baseurl}/archive/"
basename="zeromq"
custom_file_prefix="${basename}-"
