# vim:syntax=sh

latest="awk/systemd.latest.awk"
baseurl="https://github.com/systemd/systemd"
srcurl="${baseurl}/releases"
extension_input="tar.gz"
sep='"'
custom_url_prefix="${baseurl}/archive/"
basename="systemd"
custom_file_prefix="${basename}-"
