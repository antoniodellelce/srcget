# vim:syntax=sh

latest="awk/libgit2.latest.awk"
baseurl="https://github.com/libgit2/libgit2"
srcurl="${baseurl}/releases"
comment=""
extension_input="tar.gz"
sep='"'
basename="libgit2"
custom_url_prefix="${baseurl}/archive/"
custom_file_prefix="${basename}-"

## EOF ##
