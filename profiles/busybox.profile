# vim:syntax=sh

latest="awk/busybox.latest.awk"
srcurl="http://www.busybox.net/downloads"
baseurl="$srcurl"
sep='"'
extension_input="tar.bz2"
