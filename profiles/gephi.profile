# vim:syntax=sh

latest="awk/gephi.latest.awk"
baseurl="https://github.com/gephi/gephi"
srcurl="${baseurl}/releases"
extension_input="tar.gz"
sep='"'
custom_url_prefix="${baseurl}/archive/"
basename="gephi"
custom_file_prefix="${basename}-"
