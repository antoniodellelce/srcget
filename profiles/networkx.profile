# vim:syntax=sh

latest="awk/networkx.latest.awk"
baseurl="https://github.com/networkx/networkx"
srcurl="${baseurl}/releases"
extension_input="tar.gz"
sep='"'
custom_url_prefix="${baseurl}/archive/"

## EOF ##
