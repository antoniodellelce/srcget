# vim:syntax=sh

latest="awk/czmq.latest.awk"
baseurl="https://github.com/zeromq/czmq"
srcurl="${baseurl}/releases"
extension_input="tar.gz"
sep='"'
custom_url_prefix="${baseurl}/archive/"
basename="czmq"
custom_file_prefix="${basename}-"

## EOF ##
