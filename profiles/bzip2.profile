# vim:syntax=sh

latest="awk/github.latest.awk"
baseurl="https://github.com/TheApacheCats/bzip2/"
srcurl="${baseurl}/releases"
extension_input="tar.gz"
basename="bzip2"
custom_url_prefix="${baseurl}/archive/"
custom_file_prefix="${basename}-"
#custom_url_prefix="http://www.bzip.org"
#custom_file_prefix="${custom_file_prefix}"
sep='"'
