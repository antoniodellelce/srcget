# vim:syntax=sh

latest="awk/libpng.latest.awk"
srcurl="http://www.libpng.org/pub/png/libpng.html"
extension="tar.xz"
baseurl="https://vorboss.dl.sourceforge.net/project/libpng"
