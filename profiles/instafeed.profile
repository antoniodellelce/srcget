# vim:syntax=sh

latest="awk/github.latest.awk"
baseurl="https://github.com/stevenschobert/instafeed.js"
srcurl="${baseurl}/releases"
comment=""
extension_input="tar.gz"
sep='"'
basename="instafeed"
custom_url_prefix="${baseurl}/archive/"
custom_file_prefix="${basename}-"

## EOF ##
