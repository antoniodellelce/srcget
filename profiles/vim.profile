# vim:syntax=sh

latest="awk/github.latest.awk"
baseurl="https://github.com/vim/vim"
srcurl="${baseurl}/releases"
extension_input="tar.gz"
sep='"'
custom_url_prefix="${baseurl}/archive/"
basename="vim"
custom_file_prefix="${basename}-"

## EOF ##
