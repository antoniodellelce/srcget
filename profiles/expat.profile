# vim:syntax=sh

latest="awk/expat.latest.awk"
srcurl="https://sourceforge.net/projects/expat/files/expat/"
extension_input="tar.bz2"
#
#https://downloads.sourceforge.net/project/expat/expat/2.2.2/expat-2.2.2.tar.bz2
custom_url_prefix="https://netix.dl.sourceforge.net/project/expat/expat/"
custom_url_postfix=".${extension_input}"
custom_file_postfix=".${extension_input}"
