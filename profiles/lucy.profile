# vim:syntax=sh

latest="awk/lucy.latest.awk"
srcurl="http://lucy.apache.org/download.html"
baseurl="http://archive.apache.org/dist/lucy/"
comment=""
extension_input="tar.gz"
basename="apache-lucy"
custom_url_prefix="${baseurl}${basename}-"
custom_url_postfix=".${extension_input}"
custom_file_prefix="${basename}-"
custom_file_postfix=".${extension_input}"

## EOF ##
