# vim:syntax=sh

latest="awk/glib.latest.awk"
baseurl="https://github.com/GNOME/glib"
srcurl="${baseurl}/releases"
extension_input="tar.gz"
sep='"'
custom_url_prefix="${baseurl}/archive/"
basename="glib"
custom_file_prefix="${basename}-"

## EOF ##
