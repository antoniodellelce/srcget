# vim:syntax=sh

latest="awk/cmake.latest.awk"
srcurl="http://www.cmake.org/cmake/resources/software.html"
baseurl=""
extension="tar.gz"
#https://cmake.org/files/v3.12/cmake-3.12.0.tar.gz
custom_url_prefix="http://www.cmake.org/files/v"
custom_url_postfix=".${extension}"
custom_file_postfix=".${extension}"
