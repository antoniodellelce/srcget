# vim:syntax=sh

latest="awk/xz.latest.awk"
srcurl="https://tukaani.org/xz/"
sep='"'
extension_input="tar.gz"
#
custom_url_prefix="https://kent.dl.sourceforge.net/project/lzmautils/"
