# vim:syntax=sh

latest="awk/ncurses.latest.awk"
srcurl="http://ftp.gnu.org/pub/gnu/ncurses"
extension_input="tar.gz"
baseurl="$srcurl"
sep='"'
