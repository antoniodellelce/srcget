# vim:syntax=sh

latest="awk/swig.latest.awk"
srcurl="https://sourceforge.net/projects/swig/files/swig/"
extension_input="tar.gz"
#https://netix.dl.sourceforge.net/project/swig/swig/swig-3.0.12/swig-3.0.12.tar.gz
# /swig/swig-3.0.12/swig-3.0.12.tar.gz
custom_url_prefix="https://netix.dl.sourceforge.net/project/swig/swig/"
custom_url_postfix=".${extension_input}"
custom_file_postfix=".${extension_input}"
