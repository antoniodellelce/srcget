# vim:syntax=sh

latest="awk/django.latest.awk"
baseurl="https://github.com/django/django"
srcurl="${baseurl}/releases"
comment=""
extension_input="tar.gz"
sep='"'
basename="django"
custom_url_prefix="${baseurl}/archive/"
custom_file_prefix="${basename}-"

## EOF ##
