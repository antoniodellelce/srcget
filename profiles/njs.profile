# vim:syntax=sh

latest="awk/github.latest.awk"
baseurl="https://github.com/nginx/njs"
srcurl="${baseurl}/releases"
extension_input="tar.gz"
basename="njs"
sep='"'
custom_url_prefix="${baseurl}/archive/"
custom_file_prefix="${basename}-"

## EOF ##
